

declare module "ws" {
    interface WebSocket {
        send(data: any, options?: {
            compress?: boolean,
            binary?: boolean,
            fin?: boolean,
            mask?: boolean
        }, cb?: () => void)
        on(type: 'open', listener: (this: WebSocket) => void): WebSocket
        on(type: 'message', listener: (this: WebSocket, message: any, info: { masked?: boolean }) => void): WebSocket
        on(type: 'close', listener: (this: WebSocket, code: number, message: string) => void): WebSocket
        on(type: 'error', listener: (this: WebSocket, error: Error) => void): WebSocket
        close(code: number, reason?: string): void
    }
    var WebSocket: {
        new(url: string, protocals?: string | string[] | Object, option?: Object): WebSocket
        prototype: WebSocket
    }

    export = WebSocket
}

declare module "./util.js" {
    export { defer }
}