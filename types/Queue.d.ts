interface Queue<T> {
    readonly length: number
    enqueue(...item: T[]): void
    dequeue(): T | void
    remove(i: number): T | void
}

declare var Queue: {
    new <T>(...item: T[]): Queue<T>
    prototype: Queue<null>
}

export = Queue
