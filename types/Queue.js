const qLength = Symbol('Queue.length')
module.exports = class Queue {
    constructor(...items) {
        this[qLength] = 0
        if (items.length > 0)
            for (let item of items)
                this[this[qLength]++] = item
    }

    static [Symbol.hasInstance](instance) {
        return qLength in instance
    }

    enqueue(...items) {
        for (let item of items)
            this[this[qLength]++] = item
    }

    dequeue() {
        return this.remove(0)
    }

    remove(i) {
        if (this[qLength] <= 0) return
        const item = this[i]
        while (i < this[qLength])
            this[i] = this[++i]
        this[qLength]--
        return item
    }

    get length() {
        return this[qLength]
    }

    [Symbol.toPrimitive](hint) {
        switch (hint) {
            case 'string': return '[' + Array.from(this, item => item.toString()) + ']'
            case 'number': return this[qLength]
            default: return this
        }
    }

    *[Symbol.iterator]() {
        while (this[qLength] > 0)
            yield this.dequeue()
    }

    get [Symbol.toStringTag]() {
        return 'Queue'
    }
}
