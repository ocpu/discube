const seq = require('../rest/Sequential')

module.exports = class User {
    constructor() {
        this.id = null
        this.username = null
        this.discriminator = null
        this.avatar = null
        this.bot = false
        this.mfa_enabled = false
        this.verified = false
        this.email = null
    }

    static fromReady(data) {
        const user = new User()
        user.id = data.id
        user.username = data.username
        user.discriminator = data.discriminator
        user.avatar = data.avatar
        user.bot = data.bot
        user.mfa_enabled = data.mfa_enabled
        user.verified = data.verified
        user.email = data.email
        return user
    }

    static getUser(id) {
        throw new Error('Not implemented') // GET /users/{user.id}
    }

    createDM(id) {
        new seq()
    }
}