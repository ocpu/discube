module.exports = class AuthenticatedUser {
    constructor(userConstruct) {
        this.avatar = userConstruct.avatar
        this.bot = userConstruct.bot
        this.discriminator = userConstruct.discriminator
        this.email = userConstruct.email
        this.id = userConstruct.id
        this.mfaEnabled = userConstruct.mfa_enabled
        this.username = userConstruct.username
        this.verified = userConstruct.verified
    }
}