// https://discordapi.com/permissions.html#3072

const Sequential = require('./rest/Sequential')
const { Opcodes, Endpoints, Events } = require('./constants')
const WebSocket = require('ws')
const EventEmitter = require('events')
const DispatchHandler = require('./DispatchHandler')
const Shard = require('./ws/Shard')
const leftPad = require('left-pad')
const Message = require('./structures/Message')
const User = require('./structures/User')

class Client extends EventEmitter {
    /**
     * 
     * @param {string} token
     * @param {Object} options
     * @param {boolean} [options.compress=true]
     * @param {number} options.maxShards
     * @param {Array} [options.shardRange=[0, maxShards]]
     */
    constructor(token, options = {}) {
        super()
        this._shardRange = []
        if (options.shardRange) {
            if (options.shardRange[0] == void 0) {
                this._shardRange[0] = 0
            } else this._maxShards[0] = options.shardRange[0]
            if (options.shardRange[1] == void 0) {
                this._shardRange[1] == options.maxShards || this._shardRange[0] + 1
            } else this._maxShards[1] = options.shardRange[1]
        } else this._shardRange = [0, options.maxShards || 1]
        this._maxShards = options.maxShards || 1
        if (this._maxShards < this._shardRange[1])
            this._maxShards = this._maxShards
        this.token = token
        this.bot = true
        this.shards = new Array(options.maxShards || 1)
        this.dh = new DispatchHandler(this)
        this.requestHandler = new Sequential(this)
        this.guilds = []
        this._autoReconnect = true
        // this.setupEvents()
    }

    getGuild(id) {
        return this.guilds.filter(guild => guild.id === id)[0]
    }

    _dispatch(event, data) {
        switch (event) {
            case Events.READY:
                this.user = User.fromReady(data.user)
                break
        }
    }

    getGateway() {
        return this.requestHandler.request('GET', Endpoints.GATEWAY)
    }

    getBotGateway() {
        return this.requestHandler.request('GET', Endpoints.BOT_GATEWAY, true)
    }

    /**
     * Connect to discord
     * 
     * @returns {void}
     */
    connect() {
        const promise = this.bot ? this.getBotGateway() : this.getGateway()
        promise.then(data => {
            if (this.bot && this.shards.length < data.shards)
                console.warn(`WARN: The specified shard amount (${this.shards.length}) is below discords recommended shard amount (${data.shards}).
Recommended action: set the numShards option to the recommended higher amount
`)
            this.shards = this.shards.fill(void 0).map((_,i) => i).map(shardId => new Shard(data.url, shardId, this))
            this.shards.forEach(shard => shard.connect())
        })
    }

    setupUserInfo(user) {
        /*this.requestHandler.request('GET', '/users/' + user.id, true).then((reqUser) => {
            console.log(user, reqUser)
        })*/
        this.user = new AuthenticatedUser(user)
        this.emit(Events.READY, this.user)
    }
}
const client = global.client = new Client(process.env.token, {
    maxShards: 2
})

client.on('debug', ({ type, level = 'info', args = [] }) => {
    const date = new Date(Date.now() + new Date(Date.now()).getTimezoneOffset())
    console.log(`${date.getFullYear()}-${leftPad(date.getMonth(), 2, 0)}-${leftPad(date.getDate(), 2, 0)}`,
        `${date.getHours()}:${leftPad(date.getMinutes(), 2, 0)}:${leftPad(date.getSeconds(), 2, 0)}`,
        `[${type}/${level.toUpperCase()}]:`, ...args)
})

function debug(type, level, ...args) {
    const date = new Date(Date.now() + new Date(Date.now()).getTimezoneOffset())
    console.log(`${date.getFullYear()}-${leftPad(date.getMonth(), 2, 0)}-${leftPad(date.getDate(), 2, 0)}`,
        `${date.getHours()}:${leftPad(date.getMinutes(), 2, 0)}:${leftPad(date.getSeconds(), 2, 0)}`,
        `[${type}/${level.toUpperCase()}]:`, ...args)
}

//client.requestHandler.request('GET', "/users/@me/channels", true).then(console.log, console.error)

// Message.getMessage(client, '250656960246972427', '308583487315640322').then(console.log)

client.connect()