const RequestHandler = require('./RequestHandler')
const { request } = require('https')
const RateLimitQueue = require('./RateLimitQueue')
const zlib = require('zlib')

module.exports = class SequentialHandler extends RequestHandler {
    constructor(client) {
        super(client)
        this.requestQueue = new RateLimitQueue()
    }

    async handle(reqOpts, data) {
        return new Promise((resolve, reject) => {
            const req = request(reqOpts)
            this.requestQueue.enqueue(
                this.toRoute(reqOpts.path),
                next => {
                    this.requesting = true
                    req.end(data)
                    req.once('response', resp => {
                        if ('x-ratelimit-global' in resp.headers && Boolean(resp.headers['x-ratelimit-global']))
                            this.requestQueue.rateLimitGlobal(+resp.headers['retry-after'])
                        else if ('x-ratelimit-limit' in resp.headers) this.requestQueue.rateLimitRoute(
                            this.toRoute(reqOpts.path),
                            +resp.headers['x-ratelimit-limit'],
                            +resp.headers['x-ratelimit-remaining'],
                            +resp.headers['x-ratelimit-reset']
                        )

                        if (resp.statusCode == 429) {
                            return this.handle(reqOpts, data).then(resolve, reject)
                        }

                        const pipe = resp.headers['content-encoding'] === 'gzip' ? 
                            resp.pipe(zlib.createGunzip()) : 
                            resp.headers['content-encoding'] === 'deflate' ?
                                resp.pipe(zlib.createInflate()) :
                                resp

                        let bufs = []
                        pipe.on('data', d => bufs.push(d)).once('end', () => {
                            let response = JSON.parse(Buffer.concat(bufs).toString('utf-8'))
                            if (resp.statusCode >= 300) reject(new Error(resp.statusMessage))
                            resolve(response)
                        })
                        
                    })
                }
            )
        })
    }

    /*

    async _resolveRequests() {
        this.busy = true
        const request = this.requestQueue.shift()
        if (!request)
            return
        const { args, resolve, reject } = request
        const { method, url, body, auth } = args
        req.once('response', res => this._handleResponse(res, resolve, reject))
    }

    _handleResponse(req, res, resolve, reject) {
        let responseBuffers = []
        res.on('data', chunk => responseBuffers.push(chunk))
            .once('end', () => {
            const json = JSON.parse(Buffer.from(responseBuffers).toString())

            if (res.statusCode >= 400) {
                if (res.statusCode === 429) {
                    console.error(json.message)
                    if (json.global) {
                        this.requestQueue.unshift(req)
                        setTimeout(() => this._resolveRequests(), json.retry_after)
                    } else {
                        this.rateLimit[] = {
                            limit: res.headers['X-RateLimit-Limit'], 
                            remaining: res.headers['X-RateLimit-Remaining'] 
                        }
                        if (this.rateLimit.remaining != 0);
                    }

                    setTimeout(() => this.requestQueue.unshift(req), json.retry_after)
                } else reject(new Error('not a ok response'))
            }
            resolve(json)
        })
    }*/
}