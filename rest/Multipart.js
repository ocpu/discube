module.exports = class Multipart {
    constructor() {
        this._data = []
    }

    static get boundary() {
        return 'discube'
    }

    attatch(field, data, filename) {
        if (data === void 0) return this
        let head = `\r\n--${Multipart.boundary}\r\nContent-Disposition: form-data; name="${field}"`
        if (filename) head += `; filename="${filename}"`
        if (Buffer.isBuffer(data)) head += '\r\nContent-Type: application/octet-stream'
        else if (typeof data === 'object'){head += '\r\nContent-Type: application/json'
            data = new Buffer(JSON.stringify(data))
        }
        else data = new Buffer("" + data)
        this._data.push(
            new Buffer(head + '\r\n\r\n'),
            data
        )
        return this
    }

    end() {
        this._data.push(new Buffer(`\r\n--${Multipart.boundary}--`))
        return this._data
    }
}