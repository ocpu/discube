const Queue = require('../types/Queue')

let count = 0

module.exports = class RateLimitQueue extends Queue {
    constructor(...items) {
        super(...items)
        this.limits = {
            // [route]: { limit, remaining }
        }
        this.next = this.next.bind(this)
        this.requesting = false
    }

    rateLimitRoute(route, limit, remaining, release) {
        this.limits[route] = { limit, remaining }
        setTimeout(() => (this.limits[route] = void 0) && this.next(), release)
    }

    rateLimitGlobal(release) {
        this.global = true
        setTimeout(() => (this.global = false) && this.next(), release)
    }

    enqueue(route, request) {
        super.enqueue({ route, request })
        if (!this.requesting)
            this.dequeue()
    }

    dequeue() {
        console.log(++count)
        this.requesting = true
        if (!this.global) for (let i = 0; i < this.length; i++) {
            const { route, request } = this[i]
            if (route in this.limits) {
                if (this.limits[route].remaining) {
                    this.limits[route].remaining--
                    request(this.next)
                    this.remove(i)
                    return this.requesting = false
                }
            } else {
                request(this.next)
                this.remove(i)
                return this.requesting = false
            }
        }
        this.requesting = false
    }

    async next() {
        this.dequeue()
    }
}