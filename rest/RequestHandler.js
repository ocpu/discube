//const request = require('request')
const Multipart = require('./Multipart')
const { Vars } = require('../constants')
const RateLimitQueue = require('./RateLimitQueue')

module.exports = class RequestHandler {
    /**
     * 
     * @param {Client} client 
     */
    constructor(client) {
        this._client = client
        this._requesting = false
        this.rateLimited = false
        this.globalLimit = false
        this.requestQueue = new RateLimitQueue
        this.requesting
    }

    toRoute(path) {
        return path.replace(/\/([a-z-]+)\/(?:[0-9]{17,})+?/g, function(match, p) {
            return p === "channels" || p === "guilds" ? match : `/${p}/:id`;
        }).replace(/\/reactions\/.+/g, "/reactions/:id")
    }

    /**
     * 
     * @param {string} method 
     * @param {string} url
     * @param {Object} body 
     */
    request(method, url, auth, body, file) {
        if (body && method === 'GET') {
            url += '?' + Object.keys(body)
                .map(key => ({ key, value: body[key] }))
                .reduce((qs, { key, value }) => qs + Array.isArray(value) ?
                    value.reduce((total, item) => `&${encodeURIComponent(key)}=${encodeURIComponent(item)}`) :
                    `&${encodeURIComponent(key)}=${encodeURIComponent(value)}`, '')
                .substring(1)
            body = void 0
        }
        const headers = {
            'User-Agent': Vars.userAgent,
            'Accept-Encoding': 'gzip, deflate'
        }
        let data
        if (auth) headers['Authorization'] = (this._client.bot ? 'Bot ' : '') + this._client.token
        if (file) {
            const multipart = new Multipart

            headers['Content-Type'] = 'multipart/form-data; charset=utf-8; boundary=' + Multipart.boundary
            if (Array.isArray(file))
                for (let { name, data } of file)
                    multipart.attatch('file', data, name)
            else multipart.attatch('file', file.data, file.name)
            multipart.attatch('json', body)
            data = multipart.end()
        } else if (!!body) {
            headers['Content-Type'] = 'application/json'
            data = new Buffer(JSON.stringify(body))
        }
        /*const req = request({
            method,
            host: 'discordapp.com',
            path: Vars.baseUrl + url,
            headers
        })*/

        return this.handle({
            method,
            host: 'discordapp.com',
            path: Vars.baseUrl + url,
            headers
        }, data)
        /*return new Promise((resolve, reject) => {
            this.requestQueue.push({
                args: { method, url, body, auth },
                resolve,
                reject
            })
            if (!this.requesting)
                this._resolveRequests()
        })*/
    }

    _resolveRequests() {
        this.requesting = true
        const request = this.requestQueue.shift()
        if (!request)
            return
        const { args, resolve, reject } = request
        const { method, url, body, auth } = args
        const req = require('request')('https://discordapp.com/api/v6' + url, { method, body })
        req.setHeader('User-Agent', Vars.userAgent)
        req.setHeader('Authorization', (this._client.bot ? 'Bot ' : '') + this._client.token)
        req.setHeader('Content-Type', 'application/json', !!body)
        req.once('response', res => {
            let responseBuffers = []
            res.on('data', chunk => responseBuffers.push(chunk))
                .once('end', () => {
                const json = JSON.parse(Buffer.from(responseBuffers).toString())

                if (res.statusCode)
                if (res.statusCode === 429) {
                    console.error(response.message)

                    setTimeout(() => resolve(this.request(method, url, body)), response.retry_after)
                }
                resolve(response)
            })
        })
    }

    _handleResponse(res) {

    }
}