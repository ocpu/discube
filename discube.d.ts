import EventEmitter from 'events'
import WebSocket from 'ws'

namespace discube {
    interface Shard {
        socket: WebSocket
    }
    namespace rest {
        interface RequestEndpointMap {
            ''
        }
        interface RequestHandler {
            request(method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH', endpoint: string, body: string | Object, auth: boolean, file: Buffer | Buffer[]): Promise<Object>
        }
    }
    namespace constants {
        interface Endpoints {
            'GATEWAY': '/gateway'
            'BOT_GATEWAY': '/gateway/bot'
            'CHANNEL': (id: string, messageId?: boolean | string, emoji?: boolean | string, userId?: string) => string
        }
    }
    interface Client extends EventEmitter {
        readonly token: string
        readonly bot: boolean
        readonly guilds: Guild[]
        readonly requestHandler: RequestHandler
        getGateway(): Promise<{ url: string }>
        getBotGateway(): Promise<{ url: string, shards: number }>
        connect(): void
    }
    declare var Client: {
        prototype: Client
        new(token: string, options?: {
            compress?: boolean
            numShard?: number = 1
            autoReconnect?: boolean = true
        }): Client
    }
}

declare module "discube" {
    export = discube.Client
}