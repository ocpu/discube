const EventEmitter = require('events')
const WebSocket = require('ws')
const zlib = require('zlib')
const { General, Events } = require('../constants')

const Opcodes = {
    DISPATCH: 0,                // dispatches an event
    HEARTBEAT: 1,               // used for ping checking
    IDENTIFY: 2,                // used for client handshake
    STATUS_UPDATE: 3,           // used to update the client status
    VOICE_STATE_UPDATE: 4,      // used to join/move/leave voice channels
    VOICE_SERVER_PING: 5,       // used for voice ping checking
    RESUME: 6,                  // used to resume a closed connection
    RECONNECT: 7,               // used to tell clients to reconnect to the gateway
    REQUEST_GUILD_MEMBERS: 8,   // used to request guild members
    INVALID_SESSION: 9,         // used to notify client they have an invalid session id
    HELLO: 10,                  // sent immediately after connecting, contains heartbeat and server debug information
    HEARTBEAT_ACK: 11           // sent immediately following a client heartbeat that was received
}

module.exports = class Shard extends EventEmitter {
    constructor(url, id, client) {
        super()
        this.url = url
        this.id = id

        this._message = this._message.bind(this)
        this.identify = this.identify.bind(this)

        // The last sequence number recived
        this._sequence = null
        // The last session id recived
        this._sessionId = ''
        // Recived heartbeat ACK
        this._rhack = true
        // The shard socket connected to Discord servers
        this._socket
        // The whole client. Used to retreve bot/user token
        this._client = client

        // Setting up rate limmited requests with 2 per second interval
        this._requestQueue = []
        this._sendInterval = setInterval(() => {
            if (this._socket && this._socket.readyState === WebSocket.OPEN && this._requestQueue.length > 0) {
                const payload = JSON.stringify(this._requestQueue.shift())
                this._socket.send(payload)
            }
        }, 500)
    }

    /**
     * "Send" a operation code to the server.
     * 
     * Rate limited to 2 per second.
     * @param {Opcodes} op A operation code from the list at the start of the file
     * @param {*} [data=null] Any additional data
     * @returns {void}
     */
    send(op, data = null) {
        const payload = { op, d: data}
        this._requestQueue.push(payload)
    }

    connect() {
        this._socket = new WebSocket(`${this.url}?v=${General.SOCKET_API_VERSION}&encoding=${General.SOCKET_ENCODING}`)
        this._socket.on('close', (code, message) => {
            if (code > 4000)
                console.error(message)
            if (code === 4007) {
                this._sequence = null
                this._sessionId = void 0
            }
            this.reconnect()
        })
        //this._socket.on('error', this._error)
        this._socket.on('message', this._message)
    }

    _message(message) {
        if (Buffer.isBuffer(message))
            message = zlib.inflateSync(message).toString()
        const { op, d: messageData, s: sequence, t: type } = JSON.parse(message)
        switch (op) {
            case Opcodes.HELLO:
                this._heartbeatInterval = setInterval(() => {
                    if (!this._rhack) {
                        // Zombified connection
                        this.reconnect()
                        return
                    }
                    // Send heartbeat
                    this.send(Opcodes.HEARTBEAT, this._sequence)
                    // Reset the value of rhack for next acknowledgement
                    this._rhack = false
                }, messageData.heartbeat_interval)
                if (this._sequence !== null) {
                    // Resume
                    this.send(Opcodes.RESUME, {
                        token: this._client.token,
                        session_id: this._sessionId,
                        seq: this._sequence
                    })
                } else {
                    // Identify
                    this.identify()
                }
                break
            case Opcodes.HEARTBEAT_ACK: 
                this._rhack = true
                break
            case Opcodes.INVALID_SESSION:
                if (!messageData && this._client._autoReconnect) {
                    let time = Math.random() * 5000
                    if (time < 1000) time = 1000
                    setTimeout(this.identify, time)
                }
                break
            case Opcodes.DISPATCH:
                this._client.emit('debug', { type: 'Shard', args: [`Shard ${this.id} recived a ${type} event`, messageData] })
                this._sequence = sequence
                if (type === Events.READY) this.sessionId = messageData.session_id
                this._client._dispatch(type, messageData)
        }
    }

    identify() {
        this.send(Opcodes.IDENTIFY, {
            token: this._client.token,
            properties: {
                $os: process.arch,
                $browser: General.NAME,
                $device: General.NAME,
                $referrer: '',
                $referring_domain: ''
            },
            compress: true,
            large_threshold: 250,
            shard: [this.id, this._client._maxShards]
        })
    }

    reconnect() {
        this._socket.removeAllListeners()
        this._requestQueue.splice(0, this._requestQueue.length)
        clearInterval(this._heartbeatInterval)
        if (this._socket.readyState === WebSocket.OPEN)
            this._socket.close(4000)
        this.connect()
    }
}

/*



    connect() {
        const socket = this._socket = new WebSocket(`${this.url}?v=6&encoding=json`)
        socket.on('close', this._close)
        socket.on('error', this._error)
        socket.on('message', this._message)
    }

    _error(error) {
        this._client.emit('debug', { type: 'Socket', level: 'error', args: [error]})
    }

    _close(code, message) {
        // this.closed = true
        this._client.emit('debug', { type: 'Socket', args: ['Closing', { code, message }]})
        // if (code === 1006);
        //     this.send(Opcodes.RECONNECT)
    }

    _message(message) {
        const { t: event, s: sequence, op: opcode, d: data } = JSON.parse(message)
        switch (opcode) {
            case Opcodes.DISPATCH:
                this._client.emit('debug', { type: 'Socket', args: [`Recived '${event}' event`, data] })
                this._sequence = sequence
                this.emit(event, data, this)
                switch (event) {
                    case Events.READY:
                        this.sessionId = data.session_id
                        break
                    case Events.GUILD_CREATE:
                        const guild = new Guild(data)
                }
                break
            case Opcodes.HELLO:
                this._heartbeat()
                this.heartbeatInterval = setInterval(this._heartbeat.bind(this), data.heartbeat_interval)
                this.resumeOrIdentiy()
                break
            case Opcodes.INVALID_SESSION:
                this._client.emit('debug', { type: 'Socket', args: ['Recived invalid session', !data && this._client._autoReconnect ? 'now reconnecting...' : ''] })
                if (!data && this._client._autoReconnect) {
                    let time = Math.random() * 5000
                    if (time < 1000) time = 1000
                    setTimeout(this.identify, time)
                }
            case Opcodes.HEARTBEAT_ACK:
                this._rhack = true
        }
    }

    teardown() {
        
    }

    send(op, data = null) {
        const payload = { op, d: data}
        this._requestQueue.push(payload)
    }

    _heartbeat() {
        if (this._closed) return clearInterval(this.heartbeatInterval)
        if (!this._rhack) {
            this.reconnect()
            return
        }
        //this._client.emit('debug', { type: 'Socket', args: ['Sending heartbeat'] })        
        this.send(Opcodes.HEARTBEAT, this._sequence)
        this._rhack = false
    }

    resumeOrIdentiy() {
        if (this._sessionId)
            this.resume()
        else
            this.identify()
    }

    resume() {
        this.send(Opcodes.RESUME, {
            token: this._client.token,
            session_id: this._sessionId,
            seq: this._sequence
        })
    }

    identify() {
        // this.client.emit('debug', { type: 'Socket', args: ['Sending identity'] })
        this.send(Opcodes.IDENTIFY, {
            token: this._client.token,
            compress: this._client.compress,
            properties: {
                $os: process.platform
            },
            large_threshold: 250,
            shard: [this.id, this._client.shards.length]
        })
    }

*/